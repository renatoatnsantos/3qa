const gulp = require('gulp')
const imagemin = require('gulp-imagemin')
const clear = require('gulp-clean')
const concat = require('gulp-concat')
const htmlReplace = require('gulp-html-replace')
const uglify = require('gulp-uglify')
const usemin = require('gulp-usemin')
const cssmin = require('gulp-usemin')
const browserSync = require('browser-sync').create()
// bs1 = browserSync.create("first server")


gulp.task('default' , ['backup'] , ()=>{
    gulp.start('optimize-img', 'usemin' , 'server' )
})

gulp.task('optimize-img' , ()=>{
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
})

gulp.task('backup' , ['clear'] , ()=>{
    return gulp.src('src/**/*')
        .pipe(gulp.dest('dist'))
})

gulp.task('clear' , ()=>{
    return gulp.src('dist')
        .pipe(clear())
})

/*gulp.task('concat-js' , ()=>{
    gulp.src(['src/js/jquery.js' , 'src/js/bootstrap.min.js'])
        .pipe(concat('gulp-all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('src/js'))
})

gulp.task('replace-html' , ()=>{
    gulp.src('dist/!**!/!*.html')
        .pipe(htmlReplace({
            js : 'js/gulp-all.js'
        }))
        .pipe(gulp.dest('dist'))
})*/

gulp.task('usemin' , ()=>{
    gulp.src('dist/**/*.html')
        .pipe(usemin({
            'js' : [uglify],
            'css' : [cssmin]
        }))
        .pipe(gulp.dest('dist'))
})


gulp.task('server' , ()=>{
    browserSync.init({
        server : {
            baseDir : 'src' // O mesmo que http://localhost:3000 ou
            //proxy : 'http://localhost:3000' caso queira usar o gulp em conjunto a outro serv
        }
    })
    gulp.watch('src/**/*').on('change' , browserSync.reload ) // Responsável pelo live reload da página
})